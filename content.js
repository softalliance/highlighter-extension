const getNextNode = (node) => {
  if (node.firstChild) return node.firstChild;
  while (node) {
    if (node.nextSibling) return node.nextSibling;
    node = node.parentNode;
  }
};
const loadData = (user_id = null, folder = null, folder_id = null) => {
  $(document).unmark();
  $.ajax({
    url: "https://theaisquared.com/app/api/highlights/getByURL",
    type: "POST",
    data: {
      url: window.location.href,
      folder: folder,
      folder_id: folder_id,
      from: "content",
    },
    dataType: "json",
    success: function (res) {
      if (res.highlights.length) {
        $(document).unmark({
          done: function () {
            res.highlights.forEach((item) => {
              var highlightVal = JSON.parse(item.nodes);
              var selection = {
                anchorNode: elementFromQuery(highlightVal.anchorNode),
                anchorOffset: highlightVal.anchorOffset,
                focusNode: elementFromQuery(highlightVal.focusNode),
                focusOffset: highlightVal.focusOffset,
              };
              var selectionString = highlightVal.string;
              var container = elementFromQuery(highlightVal.container);
              var color = item.color.color_code;

              if (!selection.anchorNode || !selection.focusNode || !container) {
                console.log("Error: ", highlightVal);
              } else {
                highlight(
                  selectionString,
                  container,
                  selection,
                  color,
                  item.user.name,
                  item.comments,
                  item.likes,
                  item.dislikes,
                  item.id
                );
              }
            });
          },
        });

        // $(document).unmark({
        //   done: function () {
        //     res.highlights.forEach((item) => {
        //       //console.log(item,item.comments,item.comments[0].id);
        //       $(document).mark(item.content, {
        //         element:
        //           window.location.href.indexOf(".pdf") > 0 ? "span" : "mark",
        //         className: `techzarfill${item.color_id} zar-highlight`,
        //         iframes: true,
        //         separateWordSearch: false,
        //         //accuracy: "exactly",
        //         caseSensitive: true,
        //         acrossElements: true,
        //         filter: function (node, term, totalCounter, counter) {
        //           var range = document.createRange();
        //           range.selectNode(node);
        //           var position = range.getBoundingClientRect();
        //           var DOCUMENT_SCROLL_TOP =
        //             window.pageXOffset ||
        //             document.documentElement.scrollTop ||
        //             document.body.scrollTop;
        //           var top = position.top + DOCUMENT_SCROLL_TOP - 130;
        //           // console.log(getNextNode(node));
        //           return true;
        //         },
        //         each: function (el) {
        //           $(el).attr("data-user", item.user.name);
        //           $(el).attr("data-like", 0);
        //           $(el).attr("data-dislike", 0);
        //           $(el).append(`<span class="tTip">
        //                         <span class="webcmtCount"><i class="fas fa-comment-dots"></i> ${
        //                           item.comments.length === 0 ||
        //                           item.comments.length === "" ||
        //                           item.comments.length === undefined
        //                             ? 0
        //                             : item.comments.length
        //                         }</span>
        //                         <span class="webcmtLike"><i class="fas fa-thumbs-up"></i> ${
        //                           item.comments.length === 0 ||
        //                           item.comments.length === "" ||
        //                           item.comments.length === undefined
        //                             ? 0
        //                             : item.comments[0].like_count
        //                         }</span>
        //                         <span class="webcmtDislike"><i class="fas fa-thumbs-down"></i> ${
        //                           item.comments.length === 0 ||
        //                           item.comments.length === "" ||
        //                           item.comments.length === undefined
        //                             ? 0
        //                             : item.comments[0].dislike_count
        //                         }</span>
        //                         </span>
        //                 `);
        //         },
        //       });
        //     });
        //   },
        // });
      } else {
        $("mark").each(function () {
          let text = $(this).html();
          $(this).after(text);
          $(this).remove();
        });
      }
    },
    error: function (err) {
      console.log(err);
    },
  });

  $.ajax({
    // url: `https://theaisquared.com/app/api/folders/${encodeURI(folder)}`,
    url: `https://theaisquared.com/app/api/folders/id/${folder_id}`,
    headers: {
      Accept: "application/json",
    },
    dataType: "json",
    success: function (res) {
      let style = "";
      let content = "";
      // console.log(res);
      res.folder.colors.forEach((item, index) => {
        style += `
                            .techzarbtn${item.id}{
                                background-color: ${item.color_code.toUpperCase()} !important;
                                border-color: ${item.color_code.toUpperCase()} !important;
                            }
                        .techzarfill${item.id}{
                            background-color: ${item.color_code.toUpperCase()} !important;
                        }
                    `;
        // content += `<div style="display: inline-block; margin: 5px; cursor: pointer; transition: all 0.2s ease-in-out 0s; transform: scale(1);">
        //                         <button class="techzarBtn techzarbtn${item.id}" data-folder_id="${item.folder_id}" data-fill_class="techzarfill${item.id}" data-color_id="${item.id}">
        //                     </button>
        //                 </div>`;
        content += `<div style="display: inline-block; margin: 5px; cursor: pointer; transition: all 0.2s ease-in-out 0s; transform: scale(1);">
                <button style="cursor: pointer;
                  height: 20px !important;
                  line-height: 0 !important;
                  width: 20px !important;
                  min-height: 20px !important;
                  padding: 7px !important;
                  border-radius: 100px !important;
                  border-width: 1px !important;
                  border-style: solid !important;
                  border-color: transparent !important;
                  border-image: initial !important;" class="techzarBtn techzarbtn${item.id}" data-folder_id="${item.folder_id}" data-fill_class="techzarfill${item.id}" data-color_id="${item.id}" data-color_code="${item.color_code}">
              </button>
        </div>`;
        //console.log(content)
      });
      //$("body").mark("feature", { className: "techzarC1" });
      $("head").append(`<style>${style}</style>`);
      //$("body").append(`<div class="selection" style="display:none;line-height: 0; position: absolute; background-color: rgb(255, 255, 255); border-radius: 20px; transition: all 0.2s ease-in-out 0s; box-shadow: rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px; z-index: 99999;"><div>${content}</div><div class="ui-widget"><label for="tags1">Tags: </label><input id="tags1" ></div> <div style="position: absolute; border-left: 5px solid transparent; border-right: 5px solid transparent; border-top: 5px solid rgb(255, 255, 255); bottom: -4px; left: 90px; width: 0px; height: 0px;"></div></div>`);
      // $("body").append(
      //   `<div class="selection" style="display:none;line-height: 0; position: absolute; background-color: rgb(255, 255, 255); border-radius: 0px; transition: all 0.2s ease-in-out 0s; box-shadow: rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px; z-index: 99999;">
      //                       <div class="colorsDiv">
      //                           ${content}
      //                       </div>
      //                       <div style="position: absolute; border-left: 5px solid transparent; border-right: 5px solid transparent; border-top: 5px solid rgb(255, 255, 255); bottom: -4px; left: 90px; width: 0px; height: 0px;">
      //                       </div>
      //                       <div class="ui-widget">
      //                           <div class="input-container">
      //                               <input type="text" class="form-control tags_1" id="tags1" value="" placeholder="Search for...">
      //                               <span name="searchDiv" id="searchDiv-btn">
      //                                   <i class="fa fa-search" aria-hidden="true"></i>
      //                               </span>
      //                           </div>
      //                           <div id="searchDiv" style="padding: 10px;height:150px!important;overflow-y: scroll;display: none;">
      //                           </div>
      //                           <div id="extFolder">
      //                               <div class="extselectFolder">
      //                                   My research folder
      //                               </div>
      //                           </div>
      //                       </div>
      //                   </div>`
      // );
      $("div.selection").remove();
      $("body").append(
        `<div class="selection" style="display:none;line-height: 0; position: absolute; background-color: rgb(255, 255, 255); border-radius: 0px; transition: all 0.2s ease-in-out 0s; box-shadow: rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px; z-index: 99999;">
                            <div class="colorsDiv" style="text-align:center;padding:5px;">
                                ${content}
                            </div>
                            <div style="position: absolute; border-left: 5px solid transparent; border-right: 5px solid transparent; border-top: 5px solid rgb(255, 255, 255); bottom: -4px; left: 90px; width: 0px; height: 0px;">
                            </div>
                            <div class="ui-widget" style="font-family: Lato, sans-serif;font-weight: normal;font-size: 14px;padding: 0px 10px 10px;">
                                <div class="input-container" style="display: flex;width: 100%;height: 35px;">
                                    <input type="text" class="form-control tags_1" id="tags1" value="" placeholder="Search for..."
                                        style="height: auto;margin: 0px;padding: 0px 5px;">
                                    <span name="searchDiv" id="searchDiv-btn"
                                        style="
                                            cursor: pointer;
                                            position: absolute;
                                            color: rgb(255, 255, 255);
                                            width: 30px;
                                            right: 10px;
                                            text-align: center;
                                            height: 35px;
                                            line-height: 35px;
                                            background: rgb(104, 97, 206);">
                                        <img src="chrome-extension://${
                                          chrome.runtime.id
                                        }/assets/img/ext-search.png" style="width:15px;top: 3px;
    position: relative;">
                                    </span>
                                </div>
                                <div id="searchDiv" style="padding: 10px;height:150px!important;overflow-y: scroll;display: none;">
                                </div>
                                <div id="extFolder">
                                    <div class="extselectFolder"
                                        style="width: initial;
                                            height: 35px;
                                            line-height: 35px;
                                            cursor: pointer;
                                            box-shadow: rgba(12, 13, 14, 0.1) 0px 1px 2px inset;
                                            color: rgb(60, 65, 70);
                                            margin: 5px 0px 0px;
                                            background: rgb(255, 255, 255);
                                            border-width: 1px;
                                            border-style: solid;
                                            border-color: rgb(200, 204, 208);
                                            border-image: initial;
                                            padding: 0px 5px;">
                                        ${decodeURIComponent(
                                          (folder + "").replace(/\+/g, "%20")
                                        )}
                                    </div>
                                    <div style="right: 10px;
                                        position: absolute;
                                        height: 35px;
                                        line-height: 35px;
                                        background: #6861ce;
                                        color: #fff;
                                        width: 30px;
                                        text-align: center;
                                        bottom: 11px;">
                                        <img src="chrome-extension://${
                                          chrome.runtime.id
                                        }/assets/img/extfolder.png" style="width:20px;top: 2px;
    position: relative;cursor:pointer"></div>
                                </div>
                            </div>
                        </div>`
      );
      // $('#8110_btn').click(function(){
      //   $('.selection').empty();
      //   //$("head").empty();
      //   //$("head").append(`<style>${style}</style>`);
      //   $("body").append(`<div class="selection" style="display:none;line-height: 0; position: absolute; background-color: rgb(255, 255, 255); border-radius: 20px; transition: all 0.2s ease-in-out 0s; box-shadow: rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px; z-index: 99999;"><div class="ui-widget"><label for="tags1"><img src="https://lh3.googleusercontent.com/-z-XV_3AuhT4/XkzfvJOcZ0I/AAAAAAAAISM/KpLEpQNwKSYIRShlfBNAniS-PI4gWMxHQCK8BGAsYHg/s0/2020-02-18.png"  style="margin: 0px 5px; width:10px!important;" ></label><input id="tags1" placeholder="Search Here!" style="margin-right: 5px;" ><div id="8110" style="padding: 10px 14px;height:300px!important;overflow-y: scroll;"></div></div> <div style="position: absolute; border-left: 5px solid transparent; border-right: 5px solid transparent; border-top: 5px solid rgb(255, 255, 255); bottom: -4px; left: 90px; width: 0px; height: 0px;"></div></div>`);

      // });
    },
    error: function (err) {
      console.log(err);
    },
  });

  chrome.runtime.sendMessage({
    from: "content",
    subject: "loadHighlights",
  });
};

let user_id = null;
chrome.storage.sync.get(["session_id", "folder", "folder_id"], function (
  result
) {
  if (
    result.session_id != undefined &&
    result.session_id != null &&
    parseInt(result.session_id) > 0
  ) {
    // alert(result.folder);
    user_id = parseInt(result.session_id);
    var html = "";
    $.ajax({
      url: `https://theaisquared.com/app/api/folders/user/${user_id}`,
      headers: {
        Accept: "application/json",
      },
      dataType: "json",
      success: function (res) {
        res.folder.colors.forEach((item, index) => {
          html += `
          .techzarbtn${item.id}{
            background-color: ${item.color_code.toUpperCase()} !important;
            border-color: ${item.color_code.toUpperCase()} !important;
          }
          .techzarfill${item.id}{
            background-color: ${item.color_code.toUpperCase()} !important;
          }
        `;
        });
        $("head").append(`<style>${html}</style>`);
      },
      error: function (err) {
        console.log(err);
      },
    });

    var selection = new Selection();
    selection
      .config({
        colorOne: true,
        colorTwo: true,
        colorThree: true,
        colorFour: true,
        colorFive: true,
        backgroundColor: "#fff",
        iconColor: "#000",
      })
      .init();

    var selectedText = "";
    var node = "";
    window.addEventListener("mouseup", function () {
      selection = window.getSelection();
      text = selection.toString();
      selectedText = text;
    });

    $(document).on("click", ".techzarBtn", function () {
      if (selectedText) {
        var container = selection.getRangeAt(0).commonAncestorContainer;
        while (!container.innerHTML) {
          container = container.parentNode;
        }

        node = JSON.stringify({
          string: selection.toString(),
          container: getQuery(container),
          anchorNode: getQuery(selection.anchorNode),
          anchorOffset: selection.anchorOffset,
          focusNode: getQuery(selection.focusNode),
          focusOffset: selection.focusOffset,
          color: $(this).data("color_code"),
        });

        highlight(
          selectedText,
          container,
          selection,
          $(this).data("color_code")
        );
      }

      // $("body").mark(selectedText, {
      //   className: $(this).data("fill_class"),
      //   separateWordSearch: false,
      //   element: "span",
      //   acrossElements: true,
      //   iframes: true,
      // });

      $.ajax({
        url: "https://theaisquared.com/app/api/highlights",
        type: "POST",
        data: {
          folder_id: $(this).data("folder_id"),
          color_id: $(this).data("color_id"),
          content: selectedText,
          page_title: document.title,
          page_url: window.location.href,
          user_id: user_id,
          nodes: node,
        },
        success: function (res) {
          loadData(result.session_id, result.folder, res.folder_id);
          $(".selection").css("display", "none");
        },
        error: function (err) {
          console.log(err);
        },
      });
    });

    $(window).load(function () {
      loadData(result.session_id, result.folder, result.folder_id);
    });

    $(document).on("click", ".zar-highlight", function (e) {
      if (
        !$(e.target).hasClass("fa-thumbs-up") &&
        !$(e.target).hasClass("fa-thumbs-down")
      )
        toggleSidePane();
    });

    chrome.storage.onChanged.addListener(function (changes, namespace) {
      let folder_name = "";
      let folder_id = 0;
      let i = 0;
      for (var key in changes) {
        var storageChange = changes[key];
        if (key == "folder") folder_name = storageChange.newValue;
        if (key == "folder_id") folder_id = storageChange.newValue;

        if (key == "folder" || key == "folder_id") i++;
      }
      if (i >= 2) {
        loadData(result.session_id, folder_name, folder_id);
        $(".selection").hide();
      }
    });
  }
});

var iframe = document.createElement("iframe");
iframe.id = "sidePane";
// iframe.style.background = "#F2F2F2";
iframe.style.height = "100%";
iframe.style.width = "0px";
iframe.style.position = "fixed";
iframe.style.top = "0px";
iframe.style.right = "0px";
iframe.style.zIndex = "9000000000000000000";
iframe.frameBorder = "none";
iframe.src = chrome.extension.getURL(`popup.html?url=${window.location.href}`);

document.body.appendChild(iframe);

function toggleSidePane(width = "600") {
  if (iframe.style.width == "0px") {
    iframe.style.width = "600px";
  } else {
    iframe.style.width = "0px";
  }
}

$(document).on("click", "#searchDiv-btn", function () {
  var x = document.getElementById("searchDiv");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
});

chrome.runtime.onMessage.addListener((msg, sender, response) => {
  if (msg.from == "background" && msg.subject == "toggleSidePane") {
    toggleSidePane();
  }
});

$(document).on("click", function (e) {
  if (
    $(e.target).closest("mark").length > 0 ||
    $(e.target).closest("#extFolder").length > 0
  ) {
    return false;
  }
  $("#sidePane").css("width", "0px");
});

$(document).on("click", "#extFolder", function () {
  toggleSidePane();
  $(this).closest(".selection").hide();
});

$(document).on("click", ".webcmtLike", async function () {
  var highlight_id = $(this).closest("mark").data("id");
  var btn = $(this);
  var old_count = parseInt(btn.find(".count").text());
  var dislike_btn = $(this).closest("mark").find(".webcmtDislike");
  var dislike_count = parseInt(dislike_btn.find(".count").text());
  let result = await fetch(
    "https://theaisquared.com/app/api/highlights/do_like",
    {
      method: "POST",
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
      body: `like=1&id=${highlight_id}&user_id=${user_id}`,
    }
  );
  let res = await result.json();
  if (res.likes != undefined) {
    btn.find(".count").text(++old_count);
    dislike_count -= 1;
    dislike_btn.find(".count").text(dislike_count >= 0 ? dislike_count : 0);
  }
});

$(document).on("click", ".webcmtDislike", async function () {
  var highlight_id = $(this).closest("mark").data("id");
  var btn = $(this);
  var old_count = parseInt(btn.find(".count").text());
  var like_btn = $(this).closest("mark").find(".webcmtLike");
  var like_count = parseInt(like_btn.find(".count").text());
  let result = await fetch(
    "https://theaisquared.com/app/api/highlights/do_like",
    {
      method: "POST",
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
      body: `like=0&id=${highlight_id}&user_id=${user_id}`,
    }
  );
  let res = await result.json();
  if (res.dislikes != undefined) {
    btn.find(".count").text(++old_count);
    like_count -= 1;
    like_btn.find(".count").text(like_count >= 0 ? like_count : 0);
  }
});
